'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');
var path = require('path');
var cgUtils = require('../utils.js');
var chalk = require('chalk');
var _ = require('underscore');
var fs = require('fs');

_.str = require('underscore.string');
_.mixin(_.str.exports());

var ServiceGenerator = module.exports = function ServiceGenerator(args, options, config) {

    cgUtils.getNameArg(this,args);

    yeoman.generators.Base.apply(this, arguments);

};

util.inherits(ServiceGenerator, yeoman.generators.Base);

ServiceGenerator.prototype.askFor = function askFor() {
    var cb = this.async();

    var prompts = [];

    cgUtils.addNamePrompt(this,prompts,'service');

    this.prompt(prompts, function (props) {
        if (props.name){
            this.name = props.name;
        }
        cgUtils.askForModuleAndDir('service',this,false,cb);
    }.bind(this));     

};

ServiceGenerator.prototype.askForServiceType = function askFor() {
    var cb = this.async();

    var prompts = [{
        name: 'service',
        type:'list',
        message: 'What would you like to create?',
        default: 0,
        choices: ['Factory','Service']
    }];

    cgUtils.addNamePrompt(this,prompts,'service');

    this.prompt(prompts, function (props) {
        if(props.service === 'Service'){
            this.service = true;
        }
        else{
            this.service = false;
        }
        this.config.set('uirouter',this.uirouter);
        cb();
    }.bind(this));
};

ServiceGenerator.prototype.files = function files() {

    cgUtils.processTemplates(this.name,this.dir,'service',this,null,null,this.module);

};
