module <%= _.camelize(appname) %> {
    var main = angular.module('<%= _.camelize(appname) %>', ['ui.botstrap','ui.utils','<%= routerModuleName %>','ngAnimate'
        /* Inject Modules Above */
        ]);
    main.config(routeConfig);
    <% if(!uirouter){ %>
    routeConfig.$inject = ["$routeProvider"];
    function routeConfig($routeProvider: ng.route.IRouteProvider): void {

         /* Add New Routes Above */
         $routeProvider.otherwise({redirectTo:'/home'});
    }
    <% } %><% if(uirouter){ %>
    routeConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    function routeConfig($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider): void {

         /* Add New States Above */
         $urlRouterProvider.otherwise({redirectTo:'/home'});
    }
    <% } %>
}