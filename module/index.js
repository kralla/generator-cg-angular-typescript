'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');
var path = require('path');
var cgUtils = require('../utils.js');
var chalk = require('chalk');
var _ = require('underscore');
var fs = require('fs');
var tsc = require('typescript-compiler');
var ngParseModule = require('ng-parse-module');

_.str = require('underscore.string');
_.mixin(_.str.exports());

var ModuleGenerator = module.exports = function ModuleGenerator(args, options, config) {

    cgUtils.getNameArg(this,args);

    yeoman.generators.Base.apply(this, arguments);

    this.uirouter = this.config.get('uirouter');
    this.routerModuleName = this.uirouter ? 'ui.router' : 'ngRoute';

    this.on('end', function(){
        var appJs = path.join(this.dir,this.name + '.js');
        var appTs = path.join(this.dir,this.name + '.ts');
        tsc.compile([appTs],
                        '-m commonjs -t ES5 --out ' + appJs);
    });
};

util.inherits(ModuleGenerator, yeoman.generators.Base);

ModuleGenerator.prototype.askForParentModule = function askFor(){
    var cb = this.async();
    var modules = this.config.get('modules');
    var mainModule = ngParseModule.parse('app.js');
    mainModule.file = mainModule.file.replace('.js', '.ts');
    mainModule.primary = true;

    var choices = _.pluck(modules,'namespace');
    choices.unshift(mainModule.name + ' (Primary Application Module)');
    choices.unshift('None');

    var prompts = [
        {
            name:'module',
            message:'Which module should this module belong to?',
            type: 'list',
            choices: choices,
            default: 0
        }
    ];

    cgUtils.addNamePrompt(this,prompts,'module');

    this.prompt(prompts, function (props) {
        if (props.name){
            this.name = props.name;
        } 

        var i = choices.indexOf(props.module);

        var module;

        if (i === 0) {
            module = {};
            module.namespace = "";
        } else if(i === 1){
            module = mainModule;
            module.namespace = mainModule.name;
        } else {
            module = ngParseModule.parse(modules[i-2].file);
            module.file = module.file.replace('.js', '.ts');
            module.namespace = modules[i-2].namespace;
        }
        if(module.namespace !== ""){
            this.namespace = module.namespace + '.' + _.camelize(this.name);
        }
        else{
            this.namespace = _.camelize(this.name);
        }
        this.parentModule = module;
        cb();
    }.bind(this));
};

ModuleGenerator.prototype.askFor = function askFor() {
    var cb = this.async();
    var that = this;

    var defaultDir = path.dirname(that.parentModule.file);
    defaultDir = path.join(defaultDir, _.camelize(that.name), '/');
    defaultDir = path.relative(process.cwd(),defaultDir);

    var prompts = [
        {
            name:'dir',
            message:'Where would you like to create the module (must specify a subdirectory)?',
            // default: function(data){
            //     return path.join(that.name || data.name,'/');
            // },
            default: defaultDir,
            validate: function(value) {
                value = _.str.trim(value);
                if (_.isEmpty(value) || value[0] === '/' || value[0] === '\\') {
                    return 'Please enter a subdirectory.';
                }
                return true;
            }
        }
    ];

    this.prompt(prompts, function (props) {

        this.dir = path.join(props.dir,'/');
        
        cb();
    }.bind(this));
};

ModuleGenerator.prototype.files = function files() {

    var module = cgUtils.getParentModule(path.join(this.dir,'..'));
    module.dependencies.modules.push(_.camelize(this.name));
    // TODO Overwrites .ts file, use different lib
    // module.save();
    // this.log.writeln(chalk.green(' updating') + ' %s',path.basename(module.file));

    cgUtils.processTemplates(this.name,this.dir,'module',this,null,null,module);

    var modules = this.config.get('modules');
    if (!modules) {
        modules = [];
    }
    modules.push({name:_.camelize(this.name),file:path.join(this.dir,this.name + '.js'),namespace:this.namespace});
    this.config.set('modules',modules);
    this.config.save();
    if(this.parentModule.file != null){
        cgUtils.injectModule(this.parentModule.file, _.camelize(this.name), this);
    }
};